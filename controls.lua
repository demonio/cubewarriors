local state = {}
state.first = false
state.instance = nil
local functions = require("libs.functions")
local suit = require("suit")
local kenney = love.graphics.newImage("data/kenney.png")
local credits = love.graphics.newImage("data/credits.png")
local w, h = love.graphics.getDimensions()
local font = love.graphics.getFont()
suit.theme.color = {
    normal  = {bg = { 66/255, 66/255, 66/255}, fg = {188/255,188/255,188/255}},
    hovered = {bg = { 50/255,153/255,187/255}, fg = {255/255,255/255,255/255}},
    active  = {bg = {255/255,153/255,  0/255}, fg = {225/255,225/255,225/255}}
}

math.randomseed(os.time())

local input = {
  text = "Player_"..math.random(9999)
}

function state.load()
  font = love.graphics.getFont()
end

function state.update(dt)
  player_name = input.text
  w, h = love.graphics.getDimensions()
  suit.layout:reset(w/4,h/6)
  suit.layout:padding(10,10)
  suit.Label("Move:",suit.layout:row(w/2,font:getHeight()*2))
  suit.Label("Up,down,left,right (Arrows)",suit.layout:row(w/2,font:getHeight()*2))
  suit.Label("Shoot:",suit.layout:row(w/2,font:getHeight()*2))
  suit.Label("W,A,S,D)",suit.layout:row(w/2,font:getHeight()*2))
  suit.Label("General:",suit.layout:row(w/2,font:getHeight()*2))
  suit.Label("r = respawn",suit.layout:row(w/2,font:getHeight()*2))
  suit.Label("escape = exit",suit.layout:row(w/2,font:getHeight()*2))

end

function state.draw()
  suit.draw()
  love.graphics.draw(kenney, 10,(h-10)-kenney:getHeight())
  love.graphics.draw(credits,(w-10)-credits:getWidth(),(h-10)-credits:getHeight())
end

function state.textinput(t)
  suit.textinput(t)
end

function state.keypressed(key)
  if key == "escape" then
    state.instance = "menu"
  end
  if key == "n"  or key == "g" then
    state.instance = "game"
  end
  suit.keypressed(key)
end

function state.mousepressed(x, y, button)

end

return state
