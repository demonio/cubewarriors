local language = {}
local instance = "menu"
local love = love
local tostring = tostring
local string = string
local functions = require "libs.functions"
local gameinstance = require 'states'
local default_font = love.graphics.newFont( 30 )
local toggle = false
local framerate = 30
local next_time = 0
local scale = 1
local old_scale = 1
local width = 0
local height = 0

-- VERSION
local version = "Version: 0.0.4a"
-- VERSION

function love.load()
  print("Starting game")
  love.filesystem.createDirectory("maps")
  love.filesystem.createDirectory("player")
  love.graphics.setFont(default_font)
  next_time = love.timer.getTime()
end

function love.update(dt)
  next_time = next_time + 1/framerate
  width = love.graphics.getWidth( )
  height = love.graphics.getHeight( )
  scale = functions.round((height/960)*10)/10
  if scale ~= old_scale then
    print("New scale factor: "..scale)
    default_font = love.graphics.newFont( 30*scale )
    love.graphics.setFont(default_font)
    old_scale = scale
  end
  if not gameinstance[instance].first then
      gameinstance[instance].instance = instance
      gameinstance[instance].player = player
      if gameinstance[instance].load then gameinstance[instance].load() end
      gameinstance[instance].first = true
  end
  if gameinstance[instance].first and gameinstance[instance].update then
    if gameinstance[instance].updater then
      player = gameinstance[instance].player
      gameinstance[instance].updater = false
    end
    if gameinstance[instance].instance ~= instance  then
       instance = gameinstance[instance].instance
       gameinstance[instance].first = false
    end
    gameinstance[instance].update(dt)
  end
  --love.graphics.setFont(default_font)
  --print(gameinstance[instance].instance)
end

function love.draw()
  if gameinstance[instance].first and gameinstance[instance].draw then gameinstance[instance].draw() end
  love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)
  love.graphics.print(version,width/2-default_font:getWidth(version)/2,10)
  --local stats = love.graphics.getStats()

  --local str = string.format("Memory used: %.2f MB", stats.texturememory / 1024 / 1024)
  --love.graphics.print(str, 10, 40)
  local cur_time = love.timer.getTime()
  if next_time <= cur_time then
     next_time = cur_time
     return
  end
  love.timer.sleep(next_time - cur_time)
end

function love.textinput(t)
  if gameinstance[instance].textinput then gameinstance[instance].textinput(t) end
end

function love.keypressed(key, scancode, isrepeat)
  if key == "f11" then
    toggle = not toggle
    love.window.setFullscreen(toggle, "desktop")
  end
  if gameinstance[instance].keypressed then gameinstance[instance].keypressed(key, scancode, isrepeat) end
end

function love.mousepressed(x,y,b)
  if gameinstance[instance].mousepressed then gameinstance[instance].mousepressed(x, y, b) end
end

function love.mousereleased(x,y,b)
  if gameinstance[instance].mousereleased then gameinstance[instance].mousereleased(x, y, b) end
end

function love.mousemoved(x,y,mx,my)
  if gameinstance[instance].mousemoved then gameinstance[instance].mousemoved(x,y,mx,my) end
end
