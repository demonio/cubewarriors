local state = {}
state.first = false
state.instance = nil
local functions = require("libs.functions")
local network = require("libs.network")
local suit = require("suit")
local kenney = love.graphics.newImage("data/kenney.png")
local credits = love.graphics.newImage("data/credits.png")
local w, h = love.graphics.getDimensions()
local font = love.graphics.getFont()
suit.theme.color = {
    normal  = {bg = { 66/255, 66/255, 66/255}, fg = {188/255,188/255,188/255}},
    hovered = {bg = { 50/255,153/255,187/255}, fg = {255/255,255/255,255/255}},
    active  = {bg = {255/255,153/255,  0/255}, fg = {225/255,225/255,225/255}}
}

local error_msg = ""
local error_timer = 0

local buttons = {
  {
    text = "Play",
    dest = "game"
  },
  {
    text = "Controls",
    dest = "controls"
  },
  {
    text = "Quit",
    dest = "quit"
  }
}

math.randomseed(os.time())

local input = {
  text = "Player_"..math.random(9999)
}

function state.load()
  font = love.graphics.getFont()
end

function state.update(dt)
  if error_timer > 0 then
    error_timer = error_timer - dt
  end
  player_name = input.text
  w, h = love.graphics.getDimensions()
  suit.layout:reset(w/4,h/6)
  suit.layout:padding(10,10)
  suit.Label("Name:",suit.layout:row(w/2,font:getHeight()*2))
  suit.Input(input,suit.layout:row(w/2,font:getHeight()*2))
  suit.Label("Menu:",suit.layout:row(w/2,font:getHeight()*2))
  for i,v in ipairs(buttons) do
    if suit.Button(v.text, suit.layout:row(w/2,font:getHeight()*2)).hit then
      if v.dest == "game" then
        network.load()
        if not network.test() then
          error_msg = "ERROR: Server is not avaible"
          error_timer = 5
          return
        end
      end
      state.instance = v.dest
    end
  end
end

function state.draw()
  suit.draw()
  if error_timer > 0 then
    love.graphics.setColor(1,0,0)
    love.graphics.print(error_msg,10,40)
    love.graphics.setColor(1,1,1)
  end
  love.graphics.draw(kenney, 10,(h-10)-kenney:getHeight())
  love.graphics.draw(credits,(w-10)-credits:getWidth(),(h-10)-credits:getHeight())
end

function state.textinput(t)
  suit.textinput(t)
end

function state.keypressed(key)
  if key == "escape" then
    state.instance = "quit"
  end
  if key == "n"  or key == "g" then
    state.instance = "game"
  end
  suit.keypressed(key)
end

function state.mousepressed(x, y, button)

end

return state
