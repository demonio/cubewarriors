local functions = require("libs.functions")
local cpml = require "libs.cpml"
local maze = require "maze"
local iqm = require "libs.iqm"
local font = love.graphics.newFont(12)
--local text = require("text")
require("libs.class")
local dir = {
  left = {x=-1,y=0,scancode="a"},
  right = {x=1,y=0,scancode="d"},
  up = {x=0,y=-1,scancode="w"},
  down = {x=0,y=1,scancode="s"}
}

local dir2 = {
  left = {x=-1,y=0,scancode="d",k="right"},
  right = {x=1,y=0,scancode="a",k="left"},
  up = {x=0,y=-1,scancode="w",k="up"},
  down = {x=0,y=1,scancode="s",k="down"}
}

local dir4 = {
  left = {x=-1,y=0,scancode="a",k="left"},
  right = {x=1,y=0,scancode="d",k="right"},
  up = {x=0,y=-1,scancode="s",k="down"},
  down = {x=0,y=1,scancode="w",k="up"}
}

local cube  = {
  --1
  {
    left = 3,
    up = 2,
    right= 6,
    down = 5
  },
  --2
  {
    left = 6,
    right = 3,
    up = 1,
    down = 4,
  },
  --3
  {
    left = 2,
    right = 5,
    up = 1,
    down = 4,
  },
  --4
  {
    left = 3,
    right = 6,
    up = 5,
    down = 2,
  },
  --5
  {
    left = 3,
    right = 6,
    up = 1,
    down = 4,
  },
  --6
  {
    left = 2,
    right = 5,
    up = 4,
    down = 1,
  },
}

local player = class()

local bullet = {
  model = iqm.load("bullet.iqm"),
  --texture = love.graphics.newImage( "world001.png" ),
  position = cpml.vec3(0, 0, 0)
}

local function object_draw(shader,pos,rotation,object,color)
        local pos = pos or cpml.vec3(0,0,0)
        local rotation = rotation or cpml.vec3(0,0,0)
        local color = color or {1,1,1}
        --local object = object or objects["wine"]
        --local object = objects[object]
        --for _, object in ipairs(objects) do
        local m = cpml.mat4()
        m = m:translate(m,pos)
        local quat = cpml.quat.from_angle_axis (rotation.z,cpml.vec3(0,0,1))
        --m = m:rotate(m, quat)
        --m = m:rotate(m, rotation.z,cpml.vec3(0,0,1))
        --m = m:translate(m,tmp_z,cpml.vec3(0,0,1))
        --m = m:rotate(m, rotation.y,cpml.vec3(0,1,0))
        --m = m:rotate(m, rotation.x,cpml.vec3(1,0,0))
        shader:send("u_model", m:to_vec4s())
        shader:send("u_color", color)
        local complex = false
        for _, buffer in ipairs(object.model) do
                --local texture = textures[buffer.material]
                --if not texture then print("Missing texture: "..buffer.material) end
                object.model.mesh:setTexture(nil)

                object.model.mesh:setDrawRange(buffer.first, buffer.last)
                love.graphics.draw(object.model.mesh)
        end
end

function player:init(id, name,color,x,y,side,health,npc)
  print("Adding player",id, name,color,x,y,side,health,npc)
  local npc = npc or false
  local id = id or 1
  local name = name or "Demo"
  local color = color or {255/255,0,0}
  local x = x or 10
  local y = y or 10
  local side = side or 1
  local health = health or 5
  local tmp = {}
  setmetatable(tmp,player)

  tmp.bg = functions.generateBox(140,80,{0.15,0.15,0.15},0.15)
  tmp.id = id
  tmp.x = x
  tmp.y = y

  --maybe TODO
  local angle_x = 0
  local angle_y = 0
  local angle_z = 0

  if side == 1 then
    angle_x = math.pi
  elseif side == 2 then
    angle_x = math.pi/2
    angle_z = -math.pi
  elseif side == 3 then
    angle_x = math.pi/2
    angle_z = -math.pi/2
  elseif side == 4 then
  elseif side == 5 then
    angle_x = math.pi/2
  elseif side == 6 then
    angle_x = -math.pi/2
    angle_z = -math.pi/2
  end

  tmp.pressed = false
  tmp.side = side
  tmp.act_x = 0
  tmp.act_y = 0
  tmp.npc = npc
  tmp.health = health
  tmp.bullets = 5
  tmp.speed = 10
  tmp.kills = 0
  tmp.deaths = 0
  tmp.timer = 0
  tmp.timer_base = 1/15
  tmp.color = color
  tmp.name = name
  tmp.shoots = {}
  tmp.img = functions.generateBox(32,32,tmp.color)

  return tmp
end

local function swap_side(side,direction)
  local side2 = side

  --print(side)

  for i,v in ipairs(cube) do
    for k,j in pairs(dir) do
      if i == side and
        j.x == dir[direction].x and
      j.y == dir[direction].y then
        side2 = v[k]
      end
    end
  end


  return side2
end

local function swap_location(x,y,tmp_side,side,w,h)
  local tmp_x = x
  local tmp_y = y

  if (tmp_side == 3 and side == 1) or
  (tmp_side == 1 and side == 3) then
    tmp_x = y
    tmp_y = x
  end

  if (tmp_side == 6 and side == 1) then
  --(tmp_side == 1 and side == 6) then
    tmp_x = y
    tmp_y = h-x
  end

  if --(tmp_side == 6 and side == 1) then
  (tmp_side == 1 and side == 6) then
    tmp_x = w-y
    tmp_y = x
  end

  if (tmp_side == 1 and side == 5) or
  (tmp_side == 5 and side == 1) then
    tmp_x = x
    tmp_y = h-y
  end

  if (tmp_side == 6 and side == 4) or
  (tmp_side == 4 and side == 6) then
    tmp_x = y
    tmp_y = x
  end

  if (tmp_side == 3 and side == 4) then
  --(tmp_side == 4 and side == 3) then
    tmp_x = y
    tmp_y = h-x
  end

  if --(tmp_side == 6 and side == 4) or
  (tmp_side == 4 and side == 3) then
    tmp_x = w-y
    tmp_y = x
  end

  if (tmp_side == 4 and side == 2) or
  (tmp_side == 2 and side == 4) then
    tmp_x = x
    tmp_y = h-y
  end

  if (tmp_side == 6 and side == 2) or
  (tmp_side == 2 and side == 6) then
    tmp_x = w-x
    tmp_y = y
  end

  return {x = tmp_x, y = tmp_y}
end

function player:move(mapa,x,y,direction)
  local map = mapa[self.side].map


  local mapp = mapa[self.side]

  if (self.x+x) < 0 or (self.x+x) > mapp.width+1
  or (self.y+y) < 0 or (self.y+y) > mapp.height+1 then
    local tmp_side = swap_side(self.side,direction)
    if tmp_side ~= self.side then
      local tmp = swap_location(self.x,self.y,tmp_side,self.side,mapp.width+1,mapp.height+1)
      self.x = tmp.x
      self.y = tmp.y
    end
    self.side = tmp_side
    --if x == -1 and self.side == 1 then
    --  self.side = 3
    --end
    return
  end

  if not map[self.x+x] then
    self.x = self.x + x
    self.y = self.y + y
    return
  end
  if not map[self.x+x][self.y+y] then
    self.x = self.x + x
    self.y = self.y + y
    return
  end
  if map[self.x+x][self.y+y] == 0 then
    self.x = self.x + x
    self.y = self.y + y
  end
end

function player:update(dt,tile_width,tile_height)
  local tile_width = tile_width or 1
  local tile_height = tile_height or 1
  self.act_y = self.act_y - ((self.act_y - self.y*tile_height) * self.speed * dt)
	self.act_x = self.act_x - ((self.act_x - self.x*tile_width) * self.speed * dt)
end

function player:get_light(maps)
  local side = self.side
  local x = self.act_x
  local y = self.act_y
  local map = maps[self.side]
  if side == 1 then
    location = cpml.vec3(x*2,y*2, 5)
  elseif side == 2 then
    location = cpml.vec3(x*2, 5,y*2)
  elseif side == 3 then
    location = cpml.vec3(5,x*2,y*2)
  elseif side == 4 then
    location = cpml.vec3(x*2,y*2,-(2+map.width*2))
  elseif side == 5 then
    location = cpml.vec3(x*2,-map.height*2,y*2)
  elseif side == 6 then
    location = cpml.vec3(-map.width*2,x*2,y*2)
  end

  return location
end

function player:get_camera(maps)
  local side = self.side
  local x = self.act_x
  local y = self.act_y
  local map = maps[self.side]
  if side == 1 then
    location = cpml.vec3(x*2,y*2,0)
  elseif side == 2 then
    location = cpml.vec3(x*2,0,y*2)
  elseif side == 3 then
    location = cpml.vec3(0,x*2,y*2)
  elseif side == 4 then
    location = cpml.vec3(x*2,y*2,2+map.width*2)
  elseif side == 5 then
    location = cpml.vec3(x*2,2+map.height*2,y*2)
  elseif side == 6 then
    location = cpml.vec3(2+map.width*2,x*2,y*2)
  end

  return location
end

function player:get_view(maps)
  --TODO
  local side = self.side
  local x = self.act_x
  local y = self.act_y
  local map = maps[self.side]
  local angle_x = 0
  local angle_y = 0
  local angle_z = 0
  if side == 1 then
    angle_x = math.pi
  elseif side == 2 then
    angle_x = math.pi/2
    angle_z = -math.pi
  elseif side == 3 then
    angle_x = math.pi/2
    angle_z = -math.pi/2
  elseif side == 4 then
  elseif side == 5 then
    angle_x = math.pi/2
  elseif side == 6 then
    angle_x = -math.pi/2
    angle_z = -math.pi/2
  end

  return cpml.vec3(angle_x, angle_y, angle_z)
end

function player:check(dt,mapa,players,tile_width,tile_height)
  local map = mapa[self.side].map
  local tile_width = tile_width or 1
  local tile_height = tile_height or 1
  for i,v in pairs(self.shoots) do
    if v.act_x == 0 and v.act_y == 0 then
      v.act_x = v.x*tile_width
      v.act_y = v.y*tile_height
    end
    if v.timer <= 0 then
      if not map[v.x+dir[v.dir].x] then
        table.remove(self.shoots,i)
        self.bullets = self.bullets + 1
        break
      end
      if not map[v.x+dir[v.dir].x][v.y+dir[v.dir].y] then
        table.remove(self.shoots,i)
        self.bullets = self.bullets + 1
        break
      end
      for k,g in ipairs(players) do
        if g.x == v.x and g.y == v.y and g.side == self.side and g.name ~= self.name and g.health > 0 then
          table.remove(self.shoots,i)
          self.bullets = self.bullets + 1
          return g.id
        end
      end
      if map[v.x+dir[v.dir].x][v.y+dir[v.dir].y] == 0 then
        v.x = v.x + dir[v.dir].x
        v.y = v.y + dir[v.dir].y
      else
        table.remove(self.shoots,i)
        self.bullets = self.bullets + 1
        break
      end
      v.timer = v.timer_base
    else
      v.timer = v.timer - dt
    end
    v.act_y = v.act_y - ((v.act_y - v.y*tile_height) * v.speed * dt)
    v.act_x = v.act_x - ((v.act_x - v.x*tile_width) * v.speed * dt)
  end
end

function player:control(dt,mapa)
  --if self.timer <= 0 then
  if self.pressed then
    if self.side == 4 or self.side == 6 then
      if love.keyboard.isDown('down') then 	self:move(mapa,0,-1, 'down') end
      if love.keyboard.isDown('up') then 	self:move(mapa,0,1, 'up') end
    else
      if love.keyboard.isDown('up') then 	self:move(mapa,0,-1, 'up') end
      if love.keyboard.isDown('down') then 	self:move(mapa,0,1, 'down') end
    end
    if self.side == 2 then
      if love.keyboard.isDown('right') then 	self:move(mapa,-1,0, 'right') end
      if love.keyboard.isDown('left') then 	self:move(mapa,1,0, 'left') end
    else
      if love.keyboard.isDown('left') then 	self:move(mapa,-1,0, 'left') end
      if love.keyboard.isDown('right') then 	self:move(mapa,1,0, 'right') end
    end
    self.pressed = not self.pressed
  else
    if self.timer <= 0 then
      self.timer = self.timer_base
      self.pressed = not self.pressed
    else
      self.timer = self.timer - dt
    end
  end
end

function player:set_position(x,y)
  self.x = x
  self.y = y
end

function player:reset(x,y,side,health)
  local x = x or 10
  local y = y or 10
  local side = side or 1
  local health = health or 5

  self.side = side
  self.x = x
  self.y = y
  self.health = health
end

function player:shoot(direction,x,y,id)
  local tmp_bullets = self.bullets or 1
  if tmp_bullets > 0 and self.health > 0 then
    local tmp = {}
    tmp.x = x or self.x
    tmp.y = y or self.y
    tmp.id = id or 0
    tmp.act_x = 0
    tmp.act_y = 0
    tmp.speed = 10
    tmp.timer = 0
    tmp.timer_base = 1/20
    tmp.dir = direction
    table.insert(self.shoots,tmp)
    self.bullets = self.bullets - 1
    return tmp
  end
  return nil
end

local function distance ( x1, y1, x2, y2 )
   local dx = x1 - x2
   local dy = y1 - y2
   return math.sqrt ( dx * dx + dy * dy )
end

function player:ai(dt,players,maps)
  --TODO
  --BROKEN
  if not self.npc then return nil end
  local map = maps[self.side].map
  if self.timer <= 0 then
    --do actions here
    for i,v in ipairs(players) do
      --if we are on same side of cube find him and shoot
      if v.side == self.side and v.health > 0 then
        for a,b in pairs(dir) do
          if distance(v.x,v.y,self.x,self.y) > distance(v.x,v.y,self.x+b.x,self.y+b.y) then
            if (v.x == self.x or v.y ==self.y) then
              --print("ai shoot")
              self.timer = self.timer_base*8
              if self.side == 4 or self.side == 6 then
                return self:keypressed(a,dir4[a].scancode)
              elseif self.side == 2 then
                return self:keypressed(a,dir2[a].scancode)
              else
                return self:keypressed(a,b.scancode)
              end
            end
            if self.side == 4 or self.side == 6 then
              --print("BOT:",dir4[a].k)
              self:move(maps,dir4[a].x,dir4[a].y, dir4[a].k)
            elseif self.side == 2 then
              --print("BOT:",dir4[a].k)
              self:move(maps,dir2[a].x,dir2[a].y,dir2[a].k)
            else
              --print("BOT:",a)
              self:move(maps,b.x,b.y, a)
            end
          end
        end
      end
    end
    self.timer = self.timer_base*8
  else
    self.timer = self.timer - dt
  end
end

function player:keypressed(key, scancode)
  --print(scancode)
  if self.side == 4 or self.side == 6 then
    if scancode == "s" then return self:shoot("up") end
    if scancode == "w" then return self:shoot("down") end
  else
    if scancode == "s" then return self:shoot("down") end
    if scancode == "w" then return self:shoot("up") end
  end
  if self.side == 2 then
    if scancode == "d" then return self:shoot("left") end
    if scancode == "a" then return self:shoot("right") end
  else
    if scancode == "a" then return self:shoot("left") end
    if scancode == "d" then return self:shoot("right") end
  end
end

local function plane_to_cube(x,y,side,map)
  local x = x or 0
  local y = y or 0
  local side = side or 0
  local location = cpml.vec3(0,0,0)

  if side == 1 then
    location = cpml.vec3(x*2,y*2,0)
  elseif side == 2 then
    location = cpml.vec3(x*2,0,y*2)
  elseif side == 3 then
    location = cpml.vec3(0,x*2,y*2)
  elseif side == 4 then
    location = cpml.vec3(x*2,y*2,2+map.width*2)
  elseif side == 5 then
    location = cpml.vec3(x*2,2+map.height*2,y*2)
  elseif side == 6 then
    location = cpml.vec3(2+map.width*2,x*2,y*2)
  end

  return location
end

function player:draw(shader,maps,position,object)

  local x = self.act_x
  local y = self.act_y
  local side = self.side
  local map = maps[self.side]

  if self.health > 0 then
    -- model matrix (i.e. local transform)
    local location = plane_to_cube(x,y,side,map)

    local tmp_p = cpml.vec3.add(location,position)

    object_draw(shader,tmp_p,nil,object,self.color)

    for i,v in pairs(self.shoots) do
      local x = v.act_x
      local y = v.act_y
      local side = self.side
      local location = plane_to_cube(x,y,side,map)

      local tmp_p = cpml.vec3.add(location,position)

      object_draw(shader,tmp_p,nil,bullet,self.color)

      --love.graphics.draw(self.img,v.act_x,v.act_y,rotation,scale/2,scale/2,-tile_width/2,-tile_height/2)
    end
  end
end

function player:gui(x,y)
  local tmp_font = love.graphics.getFont()
  local x = x or 0
  local y = y or 0

	local radius = 100
	local mx, my = love.mouse.getPosition()

  -- TODO fix background
  love.graphics.setFont(font)
  love.graphics.draw(self.bg,x,y)
  love.graphics.draw(self.img,x+10,y+10,0,0.5,0.5)
  love.graphics.print(self.name,x+32,y+10)
  love.graphics.print("Health:"..self.health.." | Bullets:"..self.bullets,x+10,y+28)
  love.graphics.print("Kills:"..self.kills.." | Deaths:"..self.deaths,x+10,y+44)
  love.graphics.print("Side:"..self.side.." x:"..self.x.." y:"..self.y,x+10,y+60)
  love.graphics.setFont(tmp_font)
end

return player
