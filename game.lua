local game = {}
game.first = false
game.instance = nil
local iqm = require "libs.iqm"
local cpml = require "libs.cpml"
local functions = require "libs.functions"
local network = require "libs.network"
local Player = require "player"
local players = {}
-- generating map
local maze = require "maze"
local maze_x_base = 16
local maze_y_base = 16
local maze_x = maze_x_base
local maze_y = maze_x_base
local maps = {}
local network_timer = 0
local network_timer_base = 1/15
local time = 5
local textures = functions.recursiveEnumerate("data")
-- generating map

local shader = love.graphics.newShader [[
varying vec3 f_normal;

#ifdef VERTEX
attribute vec3 VertexNormal;
attribute vec4 a_Position;    // Per-vertex position information we will pass in.
attribute vec4 a_Color;       // Per-vertex color information we will pass in.
attribute vec3 a_Normal;      // Per-vertex normal information we will pass in.

uniform mat4 u_model;
uniform mat4 u_viewProj;

vec4 position(mat4 position, vec4 vertex) {
	mat4 in_u_model = transpose(u_model);
	mat4 in_u_viewProj = transpose(u_viewProj);

  f_normal = mat3(in_u_model) * VertexNormal;

	return in_u_viewProj * in_u_model * vertex;
}
#endif

#ifdef PIXEL
uniform vec4 u_light;
uniform int use_fresnel;
uniform vec3 u_color;
//uniform vec3 u_player;

const float exposure = 1.5;
const float gamma = 2.2;

vec4 effect(vec4 _col, Image s_color, vec2 _uv, vec2 _sc) {
	vec4 color  = gammaToLinear(_col);
	vec3 normal = normalize(f_normal);
	vec4 tex = texture2D(s_color, _uv);

	float shade = max(dot(normal, u_light.xyz), 0.0);
	color.rgb = shade*u_color;
	color.rgb *= u_light.w;

	// RomBinDaHouse
	color.rgb = exp( -1.0 / ( 2.72*color.rgb + 0.15 ) );
	color.rgb = pow(color.rgb, vec3(1.0 / gamma));
  color.rgb += 0.25;
  //color.rgb = vec3(1,1,1);
	color.rgb = color.rgb * u_color;
	//float tmp_x = f_normal.x - u_player.x;
	//float tmp_y = f_normal.y - u_player.y;
	//float tmp_z = f_normal.z - u_player.z;
	//float distance = sqrt(pow(tmp_x,2)+pow(tmp_y,2)+pow(tmp_z,2));

	//float alpha = 16-distance;

	return vec4(color.rgb, 1.0) * tex;
}
#endif
]]

local shader2= love.graphics.newShader [[
varying vec4 vpos;

#ifdef VERTEX
vec4 position( mat4 transform_projection, vec4 vertex_position ){
  vpos = vertex_position;
  return transform_projection * vertex_position;
}

#endif

#ifdef PIXEL
vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
  //texture_coords += vec2(cos(vpos.x), sin(vpos.y));
  vec4 texcolor = Texel(texture, texture_coords);
  return texcolor * color;
}
#endif

]]

local npc = false
local object = {
	model = iqm.load("stone.iqm"),
	--texture = functions.generateBox(1,1,{1,0,0},0),
	position = cpml.vec3(0, 0, 0)
}

local position = cpml.vec3(0,0,0)

-- view frustum's near and far clip; if far minus near is a power of two, some
-- calculations will be exact
local near, far = 0.0625, 1024.0625
local camera_fov = 105
local camera_pos = cpml.vec3(0, -5, -15)
local light_direction = cpml.vec3(0, -0.76, 0.65):normalize()
local light_intensity = 0.35
local angle = cpml.vec3(math.pi,0,0)

local dragging = false

function game.load(args)
	love.keyboard.setKeyRepeat(false)
	-- TODO each round should have different map
  for i=1,6,1 do
    local map = maze.map_maze(maze_x, maze_y, 51+i)
    maps[i] = map
  end
  network.load()
  if not network.test() then
    print("ERROR: Server is not avaible")
    os.exit()
  end

  math.randomseed(os.time())
  local x = math.random(maze_x-2)+1
  local y = math.random(maze_y-2)+1
  local side = math.random(6)
  --print(math.random(1000000))
  local tmp = network.tcp("register "..(player_name or math.random(1000000)).." "  .. x .. " ".. y.." "..side)

  players[1] = Player:init(tmp.id, tmp.name, tmp.color, tmp.x,tmp.y,tmp.side,tmp.health)
  --player = Player:init(1,"test",{0,0,1},2,2,1,5)
end

function game.update(dt)
	if not game.first then return end
	local speed = 2

  if time > 0 then
    time = time - dt
  else
    if not npc and #players < 6 then
      math.randomseed(os.time())
      local x = math.random(maze_x-2)+1
      local y = math.random(maze_y-2)+1
      local side = math.random(6)
      local tmp = network.tcp("register "..math.random(1000000).." "  .. x .. " ".. y.." "..side)
      players[#players+1] = Player:init(tmp.id, tmp.name, tmp.color, tmp.x,tmp.y,tmp.side,tmp.health,true)
      time = 5
			npc = true
    end
  end

  if (network_timer < 0) then
    network.udp_send("move "..players[1].id.." "..players[1].x.." "..players[1].y.." "..players[1].side)
    for a,b in ipairs(players) do
      if b.npc then
        network.udp_send("move "..(b.id).." "..(b.x).." "..(b.y).." "..(b.side))
      end
    end
    network_timer = network_timer_base
  else
    network_timer = network_timer - dt
  end

  local tmp = network.udp_recive()
	if tmp then
	  if tmp.player then
	    for k,v in ipairs(tmp.player) do
	      --print(k)
	      local add = true
	      for a,b in ipairs(players) do
	        if (v.id == b.id) then
						if a ~= 1 and not b.npc then
	          	b.x = v.x
	          	b.y = v.y
	          	b.side = v.side
						end
	          b.health = v.health
	          b.kills = v.kills
	          b.deaths = v.deaths
	          add = false
	        end
	      end
	      if add then
	        players[#players+1] =  Player:init(v.id,v.name,v.color,v.x,v.y,v.side)
	      end
	    end
	  end

	  if tmp.shoots then
	    for k,v in ipairs(tmp.shoots) do
	      for a,b in ipairs(players) do
	        if (v.player == b.id and a ~= 1 and not v.npc) then
	          local skip = false
	          for c,d in ipairs(b.shoots) do
	            --print("Gaga")
	            if d.id == v.id then skip = true end
	          end
	          if not skip then b:shoot(v.dir,v.x,v.y,v.id) end
	        end
	      end
	    end
	  end
	end

  for i,v in ipairs(players) do
    if i == 1 then
      v:update(dt)
      local tmp = v:check(dt,maps,players)
      if tmp then
        --print("shot")
        network.tcp("dmg "..v.id.." "..tmp.." 1")
      end
      if v.health > 0 then
        v:control(dt,maps)
      end
    else
      if v.npc then
        local tmp = v:ai(dt,players,maps)
        if tmp then
          network.tcp("shoot "..v.id.." "..tmp.x.." "..tmp.y.." "..tmp.dir)
        end
        if 1 > v.health then
          local x = math.random(maze_x-2)+1
          local y = math.random(maze_y-2)+1
          local side = math.random(6)
          v:reset(x,y,side,5)
          network.tcp("reset "..v.id.." "..v.x.." "..v.y.." "..v.side.." "..v.health)
        end
				v:update(dt)
	      local tmp = v:check(dt,maps,players)
	      if tmp then
	        --print("shot")
	        network.tcp("dmg "..v.id.." "..tmp.." 1")
	      end
			else
				v:update(dt)
	      v:check(dt,maps,players)
      end
    end
  end
  --if love.keyboard.isDown("p") then
  local tmp_p = players[1]:get_light(maps)
  light_direction = cpml.vec3(-tmp_p.x,-tmp_p.y,-tmp_p.z):normalize()
  local tmp_p = players[1]:get_camera(maps)
  position = -tmp_p
	if not dragging then
  	local tmp_p = players[1]:get_view(maps)
  	angle.x = angle.x - ((angle.x - tmp_p.x) * speed * dt)
  	angle.y = angle.y - ((angle.y - tmp_p.y) * speed * dt)
  	angle.z = angle.z - ((angle.z - tmp_p.z) * speed * dt)
  end
end

function game.mousepressed(x,y,b)
	if b == 1 then
		dragging = true
		love.mouse.setRelativeMode(true)
	end
end

function game.mousereleased(x,y,b)
	if b == 1 then
		dragging = false
		love.mouse.setRelativeMode(false)
	end
end

local sensitivity = 0.5
function game.mousemoved(x,y,mx,my)
	if dragging then
		angle.x = angle.x + math.rad(mx * sensitivity)
		angle.y = angle.y + math.rad(my * sensitivity)
	end
end

function game.keypressed(key, scancode)
  local tmp = players[1]:keypressed(key,scancode)
  if tmp then
    network.tcp("shoot "..players[1].id.." "..tmp.x.." "..tmp.y.." "..tmp.dir)
  end
  if scancode == "r" and 1 > players[1].health then
		local x = math.random(maze_x-2)+1
		local y = math.random(maze_y-2)+1
		local side = math.random(6)
    players[1]:reset(x,y,side,5)
    network.tcp("reset "..players[1].id.." "..players[1].x.." "..players[1].y.." "..players[1].side.." "..players[1].health)
  end
	if scancode == "escape" then
		game.instance = "menu"
	end
end

function draw_inside()
  for x = 1,maze_x_base do
    for y = 1,maze_x_base do
      for z = 1,maze_x_base do
        if not (x == 1 or x == maze_x_base) and
          not (y == 1 or y == maze_x_base) and
        not (z == 1 or z == maze_x_base) then
          break
        end
        local m = cpml.mat4()
        location = cpml.vec3(x*2,y*2,z*2)
        m = m:translate(m, cpml.vec3.add(location,position))
        --m = transpose(m)
        shader:send("u_model", m:to_vec4s())
        local u_color = {1,0,0}
        shader:send("u_color", u_color)

        for _, buffer in ipairs(object.model) do
          --if buffer.material then
          --print("Missing texture: "..buffer.material)
          --end
          object.model.mesh:setTexture(object.texture)
          object.model.mesh:setDrawRange(buffer.first, buffer.last)
          love.graphics.draw(object.model.mesh)
        end
      end
    end
  end
end

function game.draw()
	if not game.first then return end
	love.graphics.setShader()
  --love.graphics.setBackgroundColor(1,1,1,1)
	love.graphics.setDepthMode( "less", true)
	love.graphics.setMeshCullMode( "back" )

	-- view matrix (i.e. camera transform)
	local v = cpml.mat4()
	v:translate(v, camera_pos)
	--v = transpose(v)
  --print(angle.x,angle.y,angle.z)
	v:rotate(v, angle.x, cpml.vec3.unit_x)
	v:rotate(v, angle.y, cpml.vec3.unit_y)
	v:rotate(v, angle.z, cpml.vec3.unit_z)


	-- projection matrix (i.e. screen transform)
	local w, h = love.graphics.getDimensions()
	local p = cpml.mat4.from_perspective(camera_fov, w/h, near, far)
	--p = transpose(p)

	love.graphics.setShader(shader)
	--depth = love.graphics.getStackDepth( )
	-- update shader uniforms. set viewProj once, model for each.
	shader:send("u_viewProj", (v*p):to_vec4s())
	local tmp_p = players[1]:get_camera(maps)
	--shader:send("u_player", {tmp_p.x,tmp_p.y,tmp_p.z})
  --shader:send("u_lightPos", {position.x, position.y,position.z})
	shader:send("u_light", {
                light_direction.x, light_direction.y, light_direction.z,
		light_intensity
	})

  local side = 1
  for i,map in pairs(maps) do
    local mapa = map.map
    for x = 1,map.width do
      for y = 1,map.height do
        if mapa[x][y] ~= 0 then
          -- model matrix (i.e. local transform)
          local m = cpml.mat4()
          local location = cpml.vec3(0,0,0)
          local u_color = {1,1,1}
          if side == 1 then
            location = cpml.vec3(x*2,y*2,0)
            u_color[1] = 0
          elseif side == 2 then
            location = cpml.vec3(x*2,0,y*2)
            u_color[2] = 0
          elseif side == 3 then
            location = cpml.vec3(0,x*2,y*2)
            u_color[3] = 0
          elseif side == 4 then
            location = cpml.vec3(x*2,y*2,2+map.width*2)
            u_color[1] = 0
            u_color[3] = 0
          elseif side == 5 then
            location = cpml.vec3(x*2,2+map.height*2,y*2)
            u_color[1] = 0
            u_color[2] = 0
          elseif side == 6 then
            location = cpml.vec3(2+map.width*2,x*2,y*2)
            u_color[3] = 0
            u_color[2] = 0
          end
          m = m:translate(m, cpml.vec3.add(location,position))
          --m = transpose(m)
          shader:send("u_model", m:to_vec4s())
					u_color = {1,1,1}
          shader:send("u_color", u_color)

          for _, buffer in ipairs(object.model) do
            --if buffer.material then
            	--print("Missing texture: "..buffer.material)
            --end
            object.model.mesh:setTexture(textures[tostring(side)])
            object.model.mesh:setDrawRange(buffer.first, buffer.last)
            love.graphics.draw(object.model.mesh)
          end
        end
      end
    end
    side = side + 1
  end

  for i,v in ipairs(players) do
    v:draw(shader,maps,position,object)
  end

	-- draw 2d in foreground
	love.graphics.setShader(shader2)
  love.graphics.setDepthMode()
	love.graphics.setMeshCullMode( "none" )
	--love.graphics.print(depth,10,10)
	love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)
  --love.graphics.print((angle.x/math.pi).."|"..(angle.y/math.pi).."|"..(angle.z/math.pi),10,25)
  --love.graphics.print("Player side:"..player.side,10,40)
	--love.graphics.print("Angle X:"..angle.x.." Angle Y:"..angle.y,10,20)

  local y = 10
  local x = w - 150
  for i,v in ipairs(players) do
    if v.health > 0 then
      v:gui(x,y)
      y = y + 90
    end
  end
end

return game
