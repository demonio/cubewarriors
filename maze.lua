local functions = require("libs/functions")

local map_functions = {}

local string = string
local tonumber = tonumber
local ipairs = ipairs
local pairs = pairs
local os = os
local table = table
local io = io
local math = math

local function getopt( arg, options )
  local tab = {}
  for k, v in ipairs(arg) do
    if string.sub( v, 1, 2) == "--" then
      local x = string.find( v, "=", 1, true )
      if x then tab[ string.sub( v, 3, x-1 ) ] = string.sub( v, x+1 )
      else      tab[ string.sub( v, 3 ) ] = true
      end
    elseif string.sub( v, 1, 1 ) == "-" then
      local y = 2
      local l = string.len(v)
      local jopt
      while ( y <= l ) do
        jopt = string.sub( v, y, y )
        if string.find( options, jopt, 1, true ) then
          if y < l then
            tab[ jopt ] = string.sub( v, y+1 )
            y = l
          else
            tab[ jopt ] = arg[ k + 1 ]
          end
        else
          tab[ jopt ] = true
        end
        y = y + 1
      end
    end
  end
  return tab
end

local function swap(array, index1, index2)
   array[index1], array[index2] = array[index2], array[index1]
end

local function distance ( x1, y1, x2, y2 )
   local dx = x1 - x2
   local dy = y1 - y2
   return math.sqrt ( dx * dx + dy * dy )
end

local function shuffle(array)
   local counter = #array
   while counter > 1 do
      local index = math.random(counter)
      swap(array, index, counter)
      counter = counter - 1
   end
end

local function map_put_wall(map,x,y,wall)
   local wall = wall or "#"
   local tmp = math.random(2)
   if not map[x][y].visited and tmp == 1 then
      map[x][y].number = wall
      map[x][y].visited = true
   end
end

local function map_generate_bomber(map,wall)
  local wall = wall or "#"
  for y=1,map.height do
    for x=1,map.width do
        if (x % 2) ~= 0 and (y % 2) ~= 0 then
          map[x][y].number = wall
        end
    end
  end
end

local function map_put_se(map, air)
   local air = air or " "
   local number = 2
   local x = math.random(map.width)
   local y = math.random(map.height)
   local breaker = math.random(4)
   while number > 0 do
      if breaker == 1 then
	 x = 1
      elseif breaker == 2 then
	 y = 1
      elseif breaker == 3 then
	 x = map.width
      else
	 y = map.height
      end
      if x == 1 or y == 1 or x == map.width or y == map.height then
	 map[x][y].number = air
	 number = number - 1
	 map[x][y].se = true
	 if number == 0 then return end
      end
      x = math.random(map.width)
      y = math.random(map.height)
      local tmp = breaker
      while tmp == breaker do
	 tmp = math.random(4)
      end
      breaker = tmp
   end
end

local function smart_solve(map, x, y, kx,ky, walk,air,solved)
   local steps = 0
   local walk = walk or "9"
   local solved = solved or false
   local air = air or " "
   map[x][y].visited = true
   map[x][y].number = walk
   if x == kx and y == ky then
      return steps+1
   end
   local dir = {{x=-1,y=0,move="left"},{x=1,y=0,move="right"},{x=0,y=-1,move="up"},{x=0,y=1,move="down"}}
   local direction = math.random(#dir)

   if kx == nil or ky == nil then
      return 0
   end

   if kx < x then
      direction = 1
   elseif kx > x then
      direction = 2
   elseif ky < y then
      direction = 3
   else
      direction = 4
   end

   while #dir > 0 do
      local move_x = x+dir[direction].x
      local move_y = y+dir[direction].y
      local move = dir[direction].move
      table.remove(dir, direction)
      if move_x <= map.width and move_x >= 1 and move_y >= 1 and move_y <= map.height then
	 if (not map[move_x][move_y].visited and map[move_x][move_y].number == air ) then
	    steps = smart_solve(map,move_x,move_y,kx,ky,walk,air)
	 end
      end
      if steps > 0 then
	 return steps+1
      end
      if #dir > 0 then
	 --shuffle(dir)
	 local tmp_dir = nil
	 local base_dist = distance(x,y,kx,ky)
	 for i,v in pairs(dir) do
	    local tmp_dist = distance((x+v.x),(y+v.y),kx,ky)
	    if base_dist > tmp_dist then
	       base_dist = tmp_dist
	       tmp_dir = i
	    end
	 end
	 direction= tmp_dir or math.random(#dir)
      end
   end

   map[x][y].number = air

   return 0
end

local function map_solve2(map,walk,air)
   local walk = walk or "0"
   local air = air or " "
   local start = {}
   local konec = {}
   for x=1,map.width do
      for y=1,map.height do
	 map[x][y].visited = false
	 if map[x][y].se then
	    if not start.x then
	       start.x = x
	       start.y = y
	    else
	       konec.x = x
	       konec.y = y
	    end
	 end
      end
   end
   return smart_solve(map,start.x,start.y,konec.x,konec.y,walk,air)
end

local function backtrack(map, x, y,wall, air)
   local air = air or " "
   local wall = wall or "#"
   map[x][y].visited = true
   local dir = {{x=-1,y=0,move="left"},{x=1,y=0,move="right"},{x=0,y=-1,move="up"},{x=0,y=1,move="down"}}
   local direction = math.random(#dir)

   while #dir > 0 do
      local move_x = x+dir[direction].x
      local move_y = y+dir[direction].y
      local move = dir[direction].move
      table.remove(dir, direction)
      if (not map[move_x][move_y].visited) then --or map[move_x][move_y] ~= wall) and
      --move_x < map.width and move_x > 1 and move_y > 1 and move_y < map.height then
	 if move == "left" then
	    map_put_wall(map,move_x,move_y-1,wall)
	    map_put_wall(map,move_x,move_y+1,wall)
	 elseif move == "right" then
	    map_put_wall(map,move_x,move_y-1,wall)
	    map_put_wall(map,move_x,move_y+1,wall)
	 elseif move == "up" then
	    map_put_wall(map,move_x+1,move_y,wall)
	    map_put_wall(map,move_x-1,move_y,wall)
	 elseif move == "down" then
	    map_put_wall(map,move_x-1,move_y,wall)
	    map_put_wall(map,move_x+1,move_y,wall)
	 end
	 backtrack(map,move_x,move_y,wall)
      end
      if #dir > 0 then
	 direction= math.random(#dir)
      end
   end
end

local function map_generate_wall(map,wall)
   local wall = wall or "#"
   for x=1,map.width do
      for y=1,map.height do
	 if (x == 1 or y == 1 or x == map.width or y == map.height) and map[x][y].se == nil then
	    map[x][y].visited = true
	    map[x][y].number = wall
	 end
      end
   end
end

local function map_create(width,height,seed,air)
   local air = air or " "
   local seed = seed or os.time()
   math.randomseed(seed)
   local map = {}
   map.width = tonumber(width)
   map.height = tonumber(height)
   for x=1,width do
      map[x] = {}
      for y=1,height do
	 map[x][y] = {}
	 map[x][y] = {}
	 map[x][y].visited = false
	 map[x][y].number = air
      end
   end
   map.seed = seed
   return map
end

local function map_generate_maze(map,wall,air)
   local wall = wall or "#"
   local air = air or " "
   local x = math.random(map.width-2)+1
   local y = math.random(map.height-2)+1
   backtrack(map,x,y,wall,air)
   map_put_se(map,air)
end

local function map_draw(map,layer)
	local write = io.write
	for y=1,map.height do
		for x=1,map.width do
			write(map[x][y].number)
		end
		write("\n")
	end
end

local function map_clean(map,walk,air)
   local air = air or " "
   local walk = walk or "0"
   --print(air,walk)
   for x=1,map.width do
      for y=1,map.height do
    	 if map[x][y].number == walk then
    	    map[x][y].number = air
    	 end
      map[x][y].visited = false
      end
   end
end

local function map_table(map)
	local write = io.write
	write("return {\n")
	write("  width = "..map.width..",\n")
	write("  height = "..map.height..",\n")
	write("  seed="..map.seed..",\n")
	write("  map={\n")
	for x=1,map.width do
	   write("    {")
	   for y=1,map.height do
	      write(map[x][y].number..",")
	   end
	   write("},\n")
	end
	write("}}\n")
end

local function map_return(map)
   local tmp_map = map or {}
   tmp_map.width = map.width
   tmp_map.height = map.height
   tmp_map.seed = map.seed
   tmp_map.map = {}
   for x=1,map.width do
      tmp_map.map[x] = {}
      for y=1,map.height do
	 tmp_map.map[x][y] = map[x][y].number
      end
   end
   return tmp_map
end

local function map_generate_obstacle(map,obstacle,air,chance)
   local obstacle = obstacle or "B"
   local air = air or " "
   local chance = chance or 4

   for y=1,map.height do
      for x=1,map.width do
	 local tmp = math.random(chance)
	 if tmp == chance and map[x][y].number == air and map[x][y].se == nil then
	    map[x][y].number = obstacle
	 end
      end
   end

end


function map_functions.map_bomber(width,height,seed)
   local width = width or 64
   local height = height or 64
   local seed = seed or os.time()

   local wall = 1
   local air = 0
   local walk = 9
   local obstacle = 2
   local bonus = 4

   local map = map_create(width,height,seed,air)

   map_generate_wall(map, wall)

   map_generate_bomber(map,wall)

   map_generate_obstacle(map,obstacle,air,2)
   --map_generate_obstacle(map,bonus,air,20)

   return map_return(map), map
end

function map_functions.map_maze(width,height,seed)
   local width = width or 64
   local height = height or 64
   local seed = seed or os.time()

   local wall = 1
   local air = 0
   local walk = 9
   local obstacle = 2
   local bonus = 4

   local map = map_create(width,height,seed,air)

   map_generate_wall(map, wall)

   local dist = functions.round((distance(1,1,map.width,map.height)))

   --map = map_put_se(map, 0)

   map_generate_maze(map, wall, air)

   local pos = map_solve2(map,walk,air)
   while pos < dist do
      seed = math.random(2147483647)
      map = map_create(width,height,seed,air)
      map_generate_wall(map, wall)
      map_generate_maze(map, wall,air)
      pos = map_solve2(map,walk,air)
   end

   map_clean(map,walk,air)

   --map_generate_obstacle(map,obstacle,air,4)
   --map_generate_obstacle(map,bonus,air,20)

   return map_return(map)
end

-- Test code
local opts = getopt( arg, "ab" )

if opts["h"] or opts["help"] then
	print("USAGE: lua map.lua --wall='#' --seed=1 --width=64 --height=64")
	os.exit()
end

local width = opts["width"] or 64
local height = opts["height"] or 64
local wall = opts["wall"] or "#"
--local sky = loadstring("return"..opts["sky"])() or {1}

--print(grass,grass[1])

local seed = opts["seed"] or os.time()
--print(seed)

local wall = "@"

local air = "_"

local walk = "C"

local obstacle = "G"

--local map = map_create(width,height,seed,air)

--map_generate_wall(map, wall)

--map_put_se(map, air)

--local dist = functions.round((distance(1,1,map.width,map.height)))

--map_generate_maze(map, wall, air)

--local pos = map_solve2(map,walk,air)
--while pos < dist do
--   seed = math.random(2147483647)
--   map = map_create(width,height,seed,air)
--   map_generate_wall(map, wall)
--   map_put_se(map, air)
--   map_generate_maze(map, wall,air)
--   pos = map_solve2(map,walk,air)
--end

--map_draw(map)

--print("-----------")

--map_clean(map,walk,air)

--map_draw(map)

--print("-----------")

--map_generate_obstacle(map,obstacle,wall)

--map_generate_wall(map, wall)

--map_draw(map)

--map_table(map)

return map_functions

--map_solve2(map)

--map_draw(map)

--map_clean(map)

--map_solve(map)

--map_draw(map)

--map_clean(map)

--map_solve(map)

--map_draw(map)

--print("Map seed was "..map.seed)
